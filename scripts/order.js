import DNC3948 from "../images/dnc/3948 YN.jpg";
import DNC33 from "../images/dnc/3314450.jpg";


export const order = {
  id: "123",
  email: "david@gfc.com.au",
  name: "David Hasselhoff",
  role: "Corporate",
  company: "Geraldton Fisherman's Co-op",
  userId: "bil",
  personal: true,
  orderDate: {
    toDate: () => new Date("Tuesday, March 2, 2021, 3:39 PM"),
  },
  status: "Pending",
  total: 3900,
  comments: "Could I get this ASAP?\nI fly out tomorrow!",
  reference: "A123B12",
  items: [
    {
      code: "3948",
      product: {
        code: "3948",
        brand: "DNC",
        description: "The description",
        image: DNC33,
        name: "COTTON DRILL PANTS WITH 3M R/TAPE",
        price: 15,
        type: "Shirt",
      },
      sizes: [
        {
          amount: 5,
          colour: "Blue",
          size: "XL",
        },
      ],
      total: 36,
    },
    {
      code: "2948",
      product: {
        code: "3948",
        brand: "Visitec",
        description: "The description",
        image: DNC3948,
        name: "The Shirt",
        price: 15,
        type: "Shirt",
      },
      sizes: [
        {
          amount: 5,
          colour: "Green",
          size: "XL",
        },
      ],
      total: 36,
    },
    {
      code: "1948",
      product: {
        code: "3948",
        brand: "DNC",
        description: "The description\ncats, dog\nbabies on a different line",
        image: DNC3948,
        name: "The Shirt",
        price: 15,
        type: "Shirt",
      },
      embroidery: true,
      embroideredName: "Jim",
      sizes: [
        {
          amount: 5,
          colour: "Red",
          size: "XL",
        },
        {
          amount: 20,
          colour: "Blue",
          size: "SM",
        },
      ],
      total: 36,
    },
    {
      code: "3948",
      product: {
        code: "3948",
        brand: "DNC",
        description: "The description",
        image: DNC33,
        name: "COTTON DRILL PANTS WITH 3M R/TAPE",
        price: 15,
        type: "Shirt",
      },
      sizes: [
        {
          amount: 5,
          colour: "Blue",
          size: "XL",
        },
      ],
      total: 36,
    },
    // {
    //   code: "3948",
    //   product: {
    //     code: "3948",
    //     brand: "DNC",
    //     description: "The description",
    //     image: DNC33,
    //     name: "COTTON DRILL PANTS WITH 3M R/TAPE",
    //     price: 15,
    //     type: "Shirt",
    //   },
    //   sizes: [
    //     {
    //       amount: 5,
    //       colour: "Blue",
    //       size: "XL",
    //     },
    //   ],
    //   total: 36,
    // },
    // {
    //   code: "3948",
    //   product: {
    //     code: "3948",
    //     brand: "DNC",
    //     description: "The description",
    //     image: DNC33,
    //     name: "COTTON DRILL PANTS WITH 3M R/TAPE",
    //     price: 15,
    //     type: "Shirt",
    //   },
    //   sizes: [
    //     {
    //       amount: 5,
    //       colour: "Blue",
    //       size: "XL",
    //     },
    //   ],
    //   total: 36,
    // },
    // {
    //   code: "3948",
    //   product: {
    //     code: "3948",
    //     brand: "DNC",
    //     description: "The description",
    //     image: DNC33,
    //     name: "COTTON DRILL PANTS WITH 3M R/TAPE",
    //     price: 15,
    //     type: "Shirt",
    //   },
    //   sizes: [
    //     {
    //       amount: 5,
    //       colour: "Blue",
    //       size: "XL",
    //     },
    //   ],
    //   total: 36,
    // },
    // {
    //   code: "3948",
    //   product: {
    //     code: "3948",
    //     brand: "DNC",
    //     description: "The description",
    //     image: DNC33,
    //     name: "COTTON DRILL PANTS WITH 3M R/TAPE",
    //     price: 15,
    //     type: "Shirt",
    //   },
    //   sizes: [
    //     {
    //       amount: 5,
    //       colour: "Blue",
    //       size: "XL",
    //     },
    //   ],
    //   total: 36,
    // },
    // {
    //   code: "3948",
    //   product: {
    //     code: "3948",
    //     brand: "DNC",
    //     description: "The description",
    //     image: DNC33,
    //     name: "COTTON DRILL PANTS WITH 3M R/TAPE",
    //     price: 15,
    //     type: "Shirt",
    //   },
    //   sizes: [
    //     {
    //       amount: 5,
    //       colour: "Blue",
    //       size: "XL",
    //     },
    //   ],
    //   total: 36,
    // },
    // {
    //   code: "3948",
    //   product: {
    //     code: "3948",
    //     brand: "DNC",
    //     description: "The description",
    //     image: DNC33,
    //     name: "COTTON DRILL PANTS WITH 3M R/TAPE",
    //     price: 15,
    //     type: "Shirt",
    //   },
    //   sizes: [
    //     {
    //       amount: 5,
    //       colour: "Blue",
    //       size: "XL",
    //     },
    //   ],
    //   total: 36,
    // },
    // {
    //   code: "3948",
    //   product: {
    //     code: "3948",
    //     brand: "DNC",
    //     description: "The description",
    //     image: DNC33,
    //     name: "COTTON DRILL PANTS WITH 3M R/TAPE",
    //     price: 15,
    //     type: "Shirt",
    //   },
    //   sizes: [
    //     {
    //       amount: 5,
    //       colour: "Blue",
    //       size: "XL",
    //     },
    //   ],
    //   total: 36,
    // },
    // {
    //   code: "3948",
    //   product: {
    //     code: "3948",
    //     brand: "DNC",
    //     description: "The description",
    //     image: DNC33,
    //     name: "COTTON DRILL PANTS WITH 3M R/TAPE",
    //     price: 15,
    //     type: "Shirt",
    //   },
    //   sizes: [
    //     {
    //       amount: 5,
    //       colour: "Blue",
    //       size: "XL",
    //     },
    //   ],
    //   total: 36,
    // },
    // {
    //   code: "3948",
    //   product: {
    //     code: "3948",
    //     brand: "DNC",
    //     description: "The description",
    //     image: DNC33,
    //     name: "COTTON DRILL PANTS WITH 3M R/TAPE",
    //     price: 15,
    //     type: "Shirt",
    //   },
    //   sizes: [
    //     {
    //       amount: 5,
    //       colour: "Blue",
    //       size: "XL",
    //     },
    //   ],
    //   total: 36,
    // },
  ],
};