import $ from "jquery";
import { jsPDF } from "jspdf";
import "jspdf-autotable";
import { order } from "./order.js";
import totalUni from "../images/total-uniforms.png";
import footerImage from "../images/footer.png";
const totalPagesExp = "{total_pages_count_string}";
const colors = {
  transparent: [0, 0, 0, 0],
  white: [255, 255, 255],
  black: [0, 0, 0],
  lightgrey: [245, 245, 245],
  grey: [205, 205, 205],
  darkgrey: [120, 120, 120],
};

const tableBorder = colors.white;

const doc = new jsPDF();
doc.setLineHeightFactor(1.4);
order.items = order.items.map((item) => ({
  ...item,
  ...item.product,
}));

function centeredText(y, text) {
  var textWidth = (doc.getStringUnitWidth(text) * doc.internal.getFontSize()) / doc.internal.scaleFactor;
  var textOffset = (doc.internal.pageSize.width - textWidth) / 2;
  doc.text(textOffset, y, text);
}

function getStringWidth(text) {
  return (doc.getStringUnitWidth(text) * doc.internal.getFontSize()) / doc.internal.scaleFactor;
}

function header(width) {
  doc.setDrawColor(...colors.grey);
  doc.setLineWidth(0.1);

  doc.setFillColor(...colors.black);
  doc.rect(5, 5, width - 10, 15, "F");

  doc.setFont("helvetica");
  doc.setFontSize(20);
  doc.setTextColor(255, 255, 255);
  doc.text(15, 15, "Order Confirmation");

  doc.addImage(totalUni, width - 60, 7, 50, 10);

  return 25;
}

function details(width, offsetY) {
  doc.setFillColor(...colors.grey);
  doc.rect(5, offsetY + 2, width - 10, 7, "F");
  doc.setDrawColor(...colors.white);
  doc.line(width / 2, offsetY + 2, width / 2, offsetY + 9);

  doc.setFontSize(10);
  doc.setTextColor(...colors.black);
  doc.setFont("helvetica", "bold");
  doc.text(15, offsetY + 7, "Order Details");
  doc.text(width / 2 + 10, offsetY + 7, "Comments");
  doc.setFont("helvetica", "normal");

  doc.setFontSize(10);

  doc.setTextColor(...colors.black);
  doc.setFont("helvetica", "bold");
  doc.text(["Order #", "Date: ", "Customer: ", "Email: ", "Company: ", "Role: ", "Reference: "], 15, offsetY + 15, {
    align: "left",
  });
  doc.setFont("helvetica", "normal");
  doc.text(
    [
      order.id,
      order.orderDate.toDate().toLocaleString(),
      order.name,
      order.email,
      order.company,
      order.role,
      order.reference,
    ],
    40,
    offsetY + 15
  );
  doc.text(order.comments, width / 2 + 10, offsetY + 15);

  return offsetY + 50;
}

function orderTable(width, offsetY) {
  doc.setTextColor(...colors.black);
  doc.setFontSize(12);
  doc.setFont("helvetica", "bold");
  doc.setFont("helvetica", "normal");

  const footerHeight = 57;

  doc.autoTable({
    columns: [
      { title: "Image", dataKey: "imageDisplay" },
      { title: "Code", dataKey: "code" },
      { title: "Brand", dataKey: "brand" },
      { title: "Description", dataKey: "description" },
      { title: "Qty", dataKey: "qtyDisplay" },
      { title: "Colour", dataKey: "colourDisplay" },
      { title: "Size", dataKey: "sizesDisplay" },
      { title: "Price", dataKey: "price" },
      { title: "Total", dataKey: "total" },
    ],
    body: [...order.items],
    foot: [
      [
        { content: "", styles: { fillColor: colors.white, lineColor: colors.transparent }, colSpan: 7 },
        { content: "Subtotal", colSpan: 1 },
        { content: `$5`, colSpan: 1 },
      ],
      [
        { content: "", styles: { fillColor: colors.white, lineColor: colors.transparent }, colSpan: 7 },
        { content: "GST", colSpan: 1 },
        { content: `$5`, colSpan: 1 },
      ],
      [
        { content: "", styles: { fillColor: colors.white, lineColor: colors.transparent }, colSpan: 7 },
        { content: "Total", colSpan: 1 },
        { content: `$` + order.total, colSpan: 1 },
      ],
      [
        {
          content: "",
          styles: { fillColor: colors.transparent, lineColor: colors.transparent, minCellHeight: footerHeight },
          colSpan: 9,
        },
      ],
    ],
    theme: "striped",
    startY: offsetY + 10,
    bodyStyles: { minCellHeight: 25 },
    styles: { lineColor: tableBorder, lineWidth: 0.1 },
    columnStyles: [{ cellWidth: 25 }],
    headStyles: { fillColor: colors.grey, textColor: colors.black },
    footStyles: { fillColor: colors.grey, textColor: colors.black },
    margin: { top: 10, bottom: 15, left: 5, right: 5 },
    rowPageBreak: "avoid",
    showFoot: "lastPage",
    willDrawCell: function (data) {
      const cell = data.cell;
      if (data.section !== "body") {
        return;
      }

      const key = data.column.dataKey;
      const orderItem = data.row.raw;

      if (!orderItem.sizes) {
        return;
      }

      if (key === "qtyDisplay") {
        data.cell.text = orderItem.sizes.map((size) => "" + size.amount);
      }
      if (key === "colourDisplay") {
        data.cell.text = orderItem.sizes.map((size) => "" + size.colour);
      }
      if (key === "sizesDisplay") {
        data.cell.text = orderItem.sizes.map((size) => "" + size.size);
      }
      if (key === "price" || key === "total") {
        data.cell.text = data.cell.text.map((t) => "$" + parseInt(t).toFixed(2));
      }
    },
    didDrawCell: function (data) {
      if (data.section !== "body") {
        return;
      }

      const key = data.column.dataKey;
      const orderItem = data.row.raw;
      if (data.column.dataKey === "imageDisplay" && orderItem.image) {
        doc.addImage(orderItem.image, "JPG", data.cell.x + 5, data.cell.y + 2, 10, 20);
      }
    },
    didDrawPage: function (data) {
      var width = doc.internal.pageSize.getWidth();
      var height = doc.internal.pageSize.getHeight();
      footer(width, height);
    },
  });

  doc.putTotalPages(totalPagesExp);

  return doc.lastAutoTable.finalY - footerHeight;
}

function paymentInfo(width, offsetY) {
  doc.setDrawColor(...colors.grey);
  doc.setLineWidth(0.1);

  doc.setFontSize(12);
  doc.setFont("helvetica", "bold");
  centeredText(offsetY + 5, "Need to make a Payment?");
  doc.setFont("helvetica", "normal");
  doc.setFontSize(10);
  centeredText(offsetY + 12, "Payment can be made by one of the following methods");

  doc.setFillColor(...colors.lightgrey);
  doc.roundedRect(5, offsetY + 15, width - 10, 42, 2, 2, "F");

  doc.setFont("helvetica", "bold");
  doc.text("Direct Deposit:", 10, offsetY + 25);
  doc.setFont("helvetica", "normal");
  doc.text(["Total Uniforms", "BSB 016-650", "Acc: 432226775"], 40, offsetY + 25);

  doc.setFont("helvetica", "bold");
  doc.text("Credit Card:", width / 2, offsetY + 25);
  doc.setFont("helvetica", "normal");
  doc.text(
    ["Please call our Customer Service Team", "Monday - Friday", "08:30am - 5:00pm WST", "(08) 99653101"],
    width / 2 + 25,
    offsetY + 25
  );

  doc.setFontSize(8);
  doc.text("Please include your name or customer order on your payment", 10, offsetY + 40);
  doc.text(["To help process your order please email your remittance to: "], width / 2, offsetY + 45);

  doc.setFont("helvetica", "bold");
  doc.textWithLink("admin@totaluniforms.com.au", width / 2 + 25, offsetY + 50, {
    url: "mailto:admin@totaluniforms.com.au",
  });
  doc.setFont("helvetica", "normal");
}

function footer(width, height) {
  doc.addImage(footerImage, 5, height - 15, width - 10, 10);

  const str = doc.getNumberOfPages() + " of " + totalPagesExp;
  doc.setFontSize(10);
  doc.setTextColor(...colors.black);
  doc.text(str, width - 20, doc.internal.pageSize.height - 20);
}

function generatePdf() {
  var width = doc.internal.pageSize.getWidth();
  var height = doc.internal.pageSize.getHeight();

  let offsetY = header(width);
  offsetY = details(width, offsetY);
  offsetY = orderTable(width, offsetY);
  offsetY = paymentInfo(width, offsetY);

  return doc.output("datauristring");
}

$("iframe").attr("src", generatePdf());
